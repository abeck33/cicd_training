terraform {
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
      version = "~> 2.23"
    }
  }
}

provider "digitalocean" {
  token = var.cicd_token
}

resource "digitalocean_droplet" "cicd_deploy" {
  count  = 1
  image  = "ubuntu-22-04-x64"
  name   = "cicd-deploy"
  region = "nyc3"
  size   = "s-1vcpu-1gb"
  ipv6   = false
  ssh_keys = [36625153, 36625699, 36625705]
  resize_disk = false
  tags = ["cicddeploy"]
}

output "ip_address" {
  value = digitalocean_droplet.cicd_deploy[0].ipv4_address
  description = "The public IP address of the Droplet application."
}